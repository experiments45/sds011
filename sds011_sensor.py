"""
Read SDS01x sensor on /dev/ttyUSB0.

Read 4 samples, one sample every 20 seconds,
and print the observations on different formats.
"""

from pms.core import SensorReader
import requests


LOCATION = {'latitude': 12.93, 'longitude': 77.68}
TOKEN    = "24fb777af5a589b328fbb4fc4d2b9f500dea1bf4"
SENSORID = "BLR-APR-790"
USBPORT  = "/dev/ttyUSB0"
# sensorReadings = [   
# 	{'specie':'pm25', 'value': 393.3},  
# 	{'specie':'pm10', 'value': 109.3}  
# ] 

def send(obs):
# Station parameter   
    print(obs.pm25,obs.pm10)
    station = { 
        'id':		"BLR-APR-790",  
        'name': 	"Adarsh Palm Retreat, Bellandur",  
        'location':  { 
            'latitude': 12.93,  
            'longitude': 77.68 
        } 
    } 
    sensor_readings=[ {'specie':'pm25', 'value':obs.pm25},
                      {'specie':'pm10', 'value':obs.pm10}  ]
    # User parameter - get yours from https://aqicn.org/data-platform/token/ 
    userToken = "24fb777af5a589b328fbb4fc4d2b9f500dea1bf4" 

    # Then Upload the data  
    params = {'station':station,'readings':sensor_readings,'token':userToken}  
    request = requests.post( url = "https://aqicn.org/sensor/upload/",  json = params) 
    data = request.json()  
    if data["status"]!="ok":
        print("Something went wrong: %s" % data)
    else:
        print("Data successfully posted: %s"%data)



reader = SensorReader("SDS01x", "/dev/ttyUSB0", interval=3600, samples=1)
print("\nPMSx003 4 samples on default format")
with reader:
    for obs in reader():
        send(obs)
